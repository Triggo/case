<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function () {
        let money = $('[name="money"]');

        $('[name="convertForm"]').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: '/payment/money-to-points',
                type: 'post',
                dataType: 'json',
                data: {
                    money: money.val(),
                    _csrf: yii.getCsrfToken()
                },
                success: function (data) {
                    if (data.status === true) {
                        $('.cash').empty().append(data.money);
                        $('.points').empty().append(data.points);
                    }
                }
            });
        });
    });
</script>

<?php

/* @var $this yii\web\View */

use \yii\helpers\Html;

$this->title = 'Casexe test';

?>
<div class="site-index">

    <h1>Конвертация $ в Баллы</h1>

    <div class="body-content">
        <form method="post" name="convertForm">
            <div class="row-fluid">
                На вашем счете $<span class="cash"><?= $user->cash ?></span> и <span class="points"><?= $user->points ?></span> баллов
            </div>
            <div class="row-fluid">
                <input type="number" name="money" min="1" max="<?= $user->cash ?>">
            </div>

            <div class="row-fluid" style="margin-top: 2em;">
                <input type="submit" class="btn btn-success" value="Конвертировать">
            </div>

        </form>
    </div>
</div>

<!-- Transafer Modal -->
<div class="modal transferModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="padding: 1.5em;">
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="mt-3" name="transferForm">
                        <div class="row-fluid">
                            <input type="text" name="bank_account" class="form-control" placeholder="счет в банке">
                        </div>
                        <div class="row-fluid">
                            <div class="input-group">
                                <button class="btn btn-success cert-button bank_request">отправить выигрыш</button>
                            </div>
                        </div>
                        <div class="mt-2 row-fluid">
                            <div>Нажимая на кнопку "Записаться", я даю согласие
                                на обработку персональных данных и соглашаюсь
                                c условиями договора-оферты и политикой защиты
                                персональных данных
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Transafer Modal -->