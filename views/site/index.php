<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function (e) {
        let pre = 'Ваш выигрыш: ';
        let accept_button = $('.accept_button');
        let cancel_button = $('.cancel_button');
        let transfer_button = $('.transfer_button');
        let mail_button = $('.mail_button');
        let reward_button = $('.reward_button');

        accept_button.hide();
        cancel_button.hide();
        transfer_button.hide();
        mail_button.hide();

        reward_button.click(function () {
            $.ajax({
                url: '/reward/generate',
                type: 'post',
                dataType: 'json',
                data: {
                    _csrf: yii.getCsrfToken()
                },
                success: function (data) {
                    if (data.reward_type === 1) {
                        $('.user_reward').empty().append(pre + data.reward_qty + ' крон');
                        accept_button.show();
                        accept_button.data('target', data.reward_type);
                        accept_button.data('sum', data.reward_qty);

                        cancel_button.show();
                        cancel_button.data('target', data.reward_type);
                        cancel_button.data('sum', data.reward_qty);

                        transfer_button.show();
                        mail_button.hide();

                    } else if (data.reward_type === 2) {
                        $('.user_reward').empty().append(pre + data.reward_qty + ' баллов');
                        accept_button.show();
                        accept_button.data('target', data.reward_type);
                        accept_button.data('sum', data.reward_qty);

                        cancel_button.show();
                        cancel_button.data('target', data.reward_type);
                        cancel_button.data('sum', data.reward_qty);

                        transfer_button.hide();
                        mail_button.hide();

                    } else if (data.reward_type === 3) {
                        $('.user_reward').empty().append(pre + data.reward_qty);
                        accept_button.hide();

                        cancel_button.show();
                        cancel_button.data('target', data.reward_type);
                        cancel_button.data('sum', data.reward_qty);

                        transfer_button.hide();
                        mail_button.show();
                        mail_button.data('target', data.reward_type);
                    }
                }
            });
        });

        accept_button.click(function () {
            $.ajax({
                url: '/payment/update-user-balance',
                type: 'post',
                dataType: 'json',
                data: {
                    reward_id: $(this).data('target'),
                    sum: $(this).data('sum'),
                    _csrf: yii.getCsrfToken()
                },
                success: function (data) {
                    $('.user_reward').empty().append('ваш счет пополнен');
                    if (data.status === true) {
                        accept_button.hide();
                        cancel_button.hide();
                        transfer_button.hide();
                    }
                }
            });
        });

        cancel_button.click(function () {
            $.ajax({
                url: '/reward/cancel-reward',
                type: 'post',
                dataType: 'json',
                data: {
                    reward_id: $(this).data('target'),
                    _csrf: yii.getCsrfToken()
                },
                success: function (data) {
                    if (data.status === true) {
                        $('.user_reward').empty().append('Вы отказались от выигрыша. Сыграть еще раз?');
                        cancel_button.hide();
                        transfer_button.hide();
                    }
                }
            });
        });

        $('[name="transferForm"]').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: '/payment/send',
                type: 'post',
                dataType: 'json',
                data: {
                    _csrf: yii.getCsrfToken()
                },
                success: function (data) {
                    if (data.status === true) {
                        $('.user_reward').empty().append('Сумма отправлена');
                        accept_button.hide();
                        cancel_button.hide();
                        transfer_button.hide();
                        mail_button.hide();
                    }
                }
            });
        });

        transfer_button.click(function () {
            $('.transferModal').modal('show');
        });

        mail_button.click(function () {
            $('[name="thing_id"]').val($(this).data('target'));
            $('.shippingModal').modal('show');
        });

        $('[name="shippingForm"]').submit(function (e) {
            e.preventDefault();

            $.ajax({
                url: '/parcel/shipping',
                type: 'post',
                dataType: 'json',
                data: {
                    thing_id: $('[name="thing_id"]').val(),
                    address: $('[name="address"]').val(),
                    _csrf: yii.getCsrfToken()
                },
                success: function (data) {
                    if (data.status === true) {
                        $('.shippingModal').modal('hide');
                        $('.user_reward').empty().append('Выигрыш принят к отправке');
                        accept_button.hide();
                        cancel_button.hide();
                        transfer_button.hide();
                        mail_button.hide();
                    }
                }
            });
        });
    });
</script>

<?php

/* @var $this yii\web\View */

use \yii\helpers\Html;

$this->title = 'Casexe test';

?>
<div class="site-index">

    <div class="jumbotron">
        <?php if (!Yii::$app->user->isGuest) { ?>
            <h1>Congratulations!</h1>

            <p class="lead">Вы выиграли.</p>
            <p>
                <?=
                Html::button('Получите свой приз', [
                    'class' => 'btn btn-lg btn-success reward_button'
                ])
                ?>
            </p>
        <?php } ?>
    </div>

    <div class="body-content">
        <div class="user_reward">

        </div>
        <div class="row user_buttons">
            <button class="btn btn-success accept_button" data-target="" data-sum="">Принять</button>
            <button class="btn btn-danger cancel_button" data-target="" data-sum="">Отклонить</button>
            <button class="btn btn-info transfer_button" data-target="" data-sum="">Перевести на счет</button>
            <button class="btn btn-warning mail_button" data-target="" data-sum="">Отправить почтой</button>
        </div>
    </div>
</div>

<!-- Transafer Modal -->
<div class="modal transferModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="padding: 1.5em;">
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="mt-3" name="transferForm">
                        <div class="row-fluid">
                            <input type="text" name="bank_account" class="form-control" placeholder="счет в банке">
                        </div>
                        <div class="row-fluid">
                            <div class="input-group">
                                <button class="btn btn-success cert-button bank_request">отправить выигрыш</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Transafer Modal -->

<!-- Shipping Modal -->
<div class="modal shippingModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="padding: 1.5em;">
            <div class="modal-body">
                <div class="row-fluid">
                    <form class="mt-3" name="shippingForm">
                        <div class="row-fluid">
                            <input type="text" name="address" class="form-control" placeholder="Адрес доставки" required>
                        </div>
                        <input type="hidden" name="thing_id" value="">
                        <div class="row-fluid">
                            <div class="input-group">
                                <input type="submit" class="btn btn-success" value="отправить выигрыш">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end Shipping Modal -->