<?php

use app\models\Reward;
use yii\db\Migration;

/**
 * Handles the creation of table `reward`.
 */
class m181119_221124_create_reward_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $createdAt = $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP');
        $updatedAt = $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP');

        $this->createTable(Reward::tableName(), [
            'id' => $this->primaryKey()->unsigned(),
            'type' => $this->integer()->notNull(),
            'total' => $this->integer()->unsigned()->null()->defaultValue(null),
            'from' => $this->integer()->unsigned()->null()->defaultValue(null),
            'to' => $this->integer()->unsigned()->null()->defaultValue(null),
            'gifted_qty' => $this->integer()->unsigned()->null()->defaultValue(null),
            'available' => $this->tinyInteger()->unsigned()->defaultValue(true),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ]);

        $this->insert(Reward::tableName(), [
            'type'=> Reward::MONEY,
            'total' => 100,
            'from' => 1,
            'to' => 15,
            'gifted_qty' => 0,
            'available' => true,
        ]);

        $this->insert(Reward::tableName(), [
            'type'=> Reward::POINTS,
            'from' => 50,
            'to' => 150,
            'gifted_qty' => 0,
            'available' => true,
        ]);

        $this->insert(Reward::tableName(), [
            'type'=> Reward::THINGS,
            'total' => 3,
            'gifted_qty' => 0,
            'available' => true,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Reward::tableName());
    }
}
