<?php

use app\models\RewardUser;
use app\models\Reward;
use app\models\Thing;
use app\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `reward_user`.
 */
class m181119_224815_create_reward_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $createdAt = $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP');
        $updatedAt = $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP');
        $deletedAt = $this->timestamp()->null()->defaultValue(null);

        $this->createTable(RewardUser::tableName(), [
            'reward_id'  => $this->integer()->unsigned()->notNull(),
            'user_id'    => $this->integer()->unsigned()->notNull(),
            'thing_id'   => $this->integer()->unsigned()->null()->defaultValue(null),
            'qty'        => $this->integer()->unsigned()->notNull(),
            'cancelled'  => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
            'deleted_at' => $deletedAt,
        ]);

        // creates index for column `reward_id`
        $this->createIndex(
            'idx-reward_user-reward_id',
            'reward_user',
            'reward_id'
        );

        // add foreign key for table `reward`
        $this->addForeignKey(
            'reward_user-reward',
            RewardUser::tableName(),
            'reward_id',
            Reward::tableName(),
            'id',
            'CASCADE'
        );

        // creates index for column `user_id`
        $this->createIndex(
            'idx-reward_user-user_id',
            'reward_user',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'reward_user-user',
            RewardUser::tableName(),
            'user_id',
            User::tableName(),
            'id',
            'CASCADE'
        );

        // creates index for column `thing_id`
        $this->createIndex(
            'idx-reward_user-thing_id',
            'reward_user',
            'thing_id'
        );

        // add foreign key for table `thing`
        $this->addForeignKey(
            'reward_user-thing',
            RewardUser::tableName(),
            'thing_id',
            Thing::tableName(),
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(RewardUser::tableName());
    }
}
