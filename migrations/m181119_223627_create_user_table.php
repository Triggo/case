<?php

use app\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `user`.
 */
class m181119_223627_create_user_table extends Migration
{
    public function up()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $createdAt = $this->timestamp()->defaultExpression("CURRENT_TIMESTAMP")->notNull();
        $updatedAt = $this->timestamp()->null()->defaultExpression("NULL ON UPDATE CURRENT_TIMESTAMP");
        $deletedAt = $this->timestamp()->null()->defaultValue(null);

        $this->createTable(User::tableName(), [
            'id' => $this->primaryKey()->unsigned(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'cash' => $this->integer()->notNull()->defaultValue(0),
            'points' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
            'deleted_at' => $deletedAt,
        ], $tableOptions);

        try {
            $this->createAdmin();
        } catch (\yii\base\Exception $e) {
            Yii::error([
                'message' => 'Admin cannot exist',
                'error' => $e->getMessage()
            ]);
        }
    }

    public function down()
    {
        $this->dropTable(User::tableName());
    }

    /**
     * @throws \yii\base\Exception
     */
    public function createAdmin() {
        $model = User::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'admin';
            $user->email = 'admin@admin.com';
            $user->setPassword('admin');
            $user->generateAuthKey();
            $user->save();
        }
    }
}
