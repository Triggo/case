<?php

use app\models\Thing;
use yii\db\Migration;

/**
 * Handles the creation of table `thing`.
 */
class m181120_002849_create_thing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $createdAt = $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP');
        $updatedAt = $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP');

        $this->createTable(Thing::tableName(), [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
            'gifted'  => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
        ]);

        $items = ['pig', 'cow', 'cat'];

        foreach ($items as $item) {
            $this->insert(Thing::tableName(), [
                'name'=> $item,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Thing::tableName());
    }
}
