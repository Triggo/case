<?php

use app\models\Thing;
use app\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `parcel`.
 */
class m181123_142932_create_parcel_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $createdAt = $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP');
        $updatedAt = $this->timestamp()->null()->defaultExpression('NULL ON UPDATE CURRENT_TIMESTAMP');
        $deletedAt = $this->timestamp()->null()->defaultValue(null);

        $this->createTable('parcel', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'thing_id' => $this->integer()->unsigned()->notNull(),
            'shipping' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1),
            'delivered' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0),
            'address' => $this->string()->notNull(),
            'created_at' => $createdAt,
            'updated_at' => $updatedAt,
            'deleted_at' => $deletedAt,
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-parcel-user_id',
            'parcel',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'parcel-user',
            'parcel',
            'user_id',
            User::tableName(),
            'id',
            'CASCADE'
        );

        // creates index for column `thing_id`
        $this->createIndex(
            'idx-parcel-thing_id',
            'parcel',
            'thing_id'
        );

        // add foreign key for table `thing`
        $this->addForeignKey(
            'parcel-thing',
            'parcel',
            'thing_id',
            Thing::tableName(),
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('parcel');
    }
}
