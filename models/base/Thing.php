<?php
namespace app\models\base;

use yii\db\ActiveRecord;

/**
 * Class Thing
 * @package app\models\base
 *
 * @property int $id
 * @property string $name
 * @property int $gifted
 * @property string $created_at
 * @property string $updated_at
 */
class Thing extends ActiveRecord
{
    public const AVAILABLE = 0;
    public const GIFTED = 1;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%thing}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gifted'], 'integer'],
            [['name'], 'string'],
            [['name'], 'required'],
            [['created_at'], 'safe'],
        ];
    }
}