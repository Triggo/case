<?php
namespace app\models\base;

use yii\db\ActiveRecord;
use app\models\Reward;
use app\models\User;
use app\models\Thing;

/**
 * Class RewardUser
 * @package app\models\base
 *
 * @property int $reward_id
 * @property int $user_id
 * @property int $thing_id
 * @property int $qty
 * @property int $cancelled
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * @property Reward $reward
 * @property User $user
 * @property Thing $thing
 */
class RewardUser extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%reward_user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reward_id', 'user_id', 'thing_id', 'qty'], 'integer'],
            [['reward_id', 'user_id', 'qty'], 'required'],
            [['created_at'], 'safe'],
        ];
    }
}