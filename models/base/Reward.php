<?php
namespace app\models\base;

use yii\db\ActiveRecord;

/**
 * Class Reward
 * @package app\models\base
 *
 * @property int $id
 * @property string $name
 * @property int $total
 * @property int $from
 * @property int $to
 * @property int $gifted_qty
 * @property string $created_at
 * @property string $updated_at
 */
class Reward extends ActiveRecord
{
    public const MONEY  = 1;
    public const POINTS = 2;
    public const THINGS = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%reward}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['total', 'from', 'to', 'gifted_qty'], 'integer'],
            [['created_at'], 'safe'],
        ];
    }
}