<?php
namespace app\models\base;

use yii\db\ActiveRecord;

/**
 * Class Parcel
 * @package app\models\base
 *
 * @property int $id
 * @property string $user_id
 * @property string $thing_id
 * @property string $address
 * @property string $created_at
 * @property string $updated_at
 */
class Parcel extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%parcel}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'thing_id'], 'integer'],
            [['address'], 'string'],
            [['user_id', 'thing_id'], 'required'],
            [['created_at'], 'safe'],
        ];
    }
}