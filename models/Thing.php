<?php
namespace app\models;


use yii\db\ActiveQuery;

class Thing extends base\Thing
{
    /**
     * @return Thing
     */
    public static function getNonGifted(): Thing
    {
        return self::find()
            ->where(['gifted' => 0])
            ->orderBy('RAND()')->one();
    }
}