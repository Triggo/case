<?php
namespace app\models;

/**
 * Class Reward
 * @package app\models
 *
 * @property int $id
 * @property string $name
 * @property int $type
 * @property int $available
 * @property int $from
 * @property int $to
 * @property int $gifted_qty
 * @property int $total
 */
class Reward extends base\Reward
{
    /**
     * @return Reward
     */
    public static function getAvailable(): Reward
    {
        return self::find()
            ->where(['available' => 1])
            ->orderBy('RAND()')->one();
    }
}