<?php
namespace app\controllers;


use app\models\Parcel;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class ParcelController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['shipping'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'shipping' => ['POST']
                ],
            ],
        ];
    }

    public function actionShipping()
    {
        $params = Yii::$app->request->post();

        try {
            $parcel = new Parcel();
            $parcel->user_id = Yii::$app->user->id;
            $parcel->thing_id = $params['thing_id'];
            $parcel->address = $params['address'];

            $parcel->save();
        } catch (\Exception $e) {
            Yii::error([
                'method' => 'something wrong',
                'error' => $e->getMessage(),
            ]);
            return \yii\helpers\Json::encode(['status' => false]);
        }

        return \yii\helpers\Json::encode(['status' => true]);
    }
}