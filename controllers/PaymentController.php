<?php

namespace app\controllers;


use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class PaymentController extends Controller
{
    private const BANK_API_URL = 'https://api.bank.boo';

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['send', 'update-user-balance', 'convert', 'money-to-points'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'send' => ['POST'],
                    'convert' => ['GET'],
                    'update-user-balance' => ['POST'],
                    'money-to-points' => ['POST'],
                ],
            ],
        ];
    }

    public function actionSend()
    {
        $params = Yii::$app->request->post();

        try {
            $client = new \GuzzleHttp\Client();
//            $client->request('POST', BANK_API_URL, ['query' => [
//                'bank_account' => $params['bank_account'],
//                'sum' => $params['sum'],
//            ]]);
        } catch (\Exception $e) {
            Yii::error([
                'method' => 'something wrong',
                'error' => $e->getMessage(),
            ]);
            return \yii\helpers\Json::encode(['status' => false]);
        }

        return \yii\helpers\Json::encode(['status' => true]);
    }

    public function actionUpdateUserBalance()
    {
        $params = Yii::$app->request->post();
        $user = User::findIdentity(Yii::$app->user->id);

        if (is_null($user)) {
            Yii::error([
                'method' => 'User not found',
            ]);
            return \yii\helpers\Json::encode(['status' => false]);
        }

        if ($params['reward_id'] == 1) {
            $user->cash = $user->cash + $params['sum'];
        } elseif ($params['reward_id'] == 2) {
            $user->points = $user->points + $params['sum'];
        }

        try {
            $user->save();
        } catch (\Exception $e) {
            Yii::error([
                'error' => $e->getMessage(),
            ]);
            return \yii\helpers\Json::encode(['status' => false]);
        }

        return \yii\helpers\Json::encode(['status' => true]);
    }

    /**
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionConvert()
    {
        $user = User::findIdentity(Yii::$app->user->id);

        if (is_null($user)) {
            Yii::error([
                'method' => 'User not found',
            ]);
            throw new NotFoundHttpException('User not found');
        }

        return $this->render('convert', compact('user'));
    }

    public function actionMoneyToPoints()
    {
        $params = Yii::$app->request->post();
        $user = User::findIdentity(Yii::$app->user->id);
        $convertedSum = $params['money'];
        $percentage = 30;

        if (is_null($user)) {
            Yii::error([
                'method' => 'User not found',
            ]);
            return \yii\helpers\Json::encode(['status' => false]);
        }

        $commission = ($percentage / 100) * $convertedSum;

        $newCash = $user->cash - $convertedSum;
        $newPoints = $user->points + round($convertedSum - $commission);

        User::updateAll([
            'cash' => $newCash,
            'points' => $newPoints
        ], [
            'id' => $user->id
        ]);

        Yii::$app->db->getLastInsertID();

        return \yii\helpers\Json::encode([
            'status' => true,
            'money' => $newCash,
            'points' => $newPoints,
        ]);
    }

}