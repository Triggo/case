<?php

namespace app\controllers;


use app\models\Reward;
use app\models\RewardUser;
use app\models\Thing;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Class RewardController
 * @package app\controllers
 *
 * @property Thing $thing
 */
class RewardController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'actions' => ['generate', 'cancel-reward'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'generate' => ['POST'],
                    'cancel-reward' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionGenerate()
    {
        $reward = $this->getReward();

        return \yii\helpers\Json::encode($reward);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function actionCancelReward()
    {
        $params = Yii::$app->request->post();
        $userId = Yii::$app->user->id;

        try {
            RewardUser::updateAll([
                'cancelled' => 1
            ], [
                'reward_id' => $params['reward_id'],
                'user_id' => $userId
            ]);
        } catch (\Exception $e) {
            Yii::error([
                'message' => $e->getMessage()
            ]);
            return \yii\helpers\Json::encode(['status' => false]);
        }

        return \yii\helpers\Json::encode(['status' => true]);
    }

    /**
     * @return array|bool
     * @throws \Exception
     */
    private function getReward()
    {
        $reward_qty = 0;
        $user = User::findOne(Yii::$app->user->id);

        $reward = Reward::getAvailable();

        if (is_null($reward)) {
            Yii::error([
                'message' => 'rewards not found'
            ]);
            return false;
        }

        if ($reward->type == Reward::MONEY) {

            $delta = $reward->total - $reward->gifted_qty;

            if ($delta > $reward->to) {
                $reward_qty = $this->getRewardSize($reward->from, $reward->to);
            } else {
                $reward_qty = $this->getRewardSize($reward->from, $delta);
            }

            $this->updateGiftedQty($reward, $reward_qty);
            $this->createRewardUserAssociate($reward, $user, $reward_qty);

        } elseif ($reward->type == Reward::POINTS) {

            $reward_qty = $this->getRewardSize($reward->from, $reward->to);
            $this->updateGiftedQty($reward, $reward_qty);
            $this->createRewardUserAssociate($reward, $user, $reward_qty);

        } elseif ($reward->type == Reward::THINGS) {

            $thing = $this->getAvailableThing($reward);
            $this->createRewardUserAssociate($reward, $user, 1, $thing->id);
            $reward_qty = $thing->name;
        }

        $result = [
            'reward_type' => $reward->type,
            'reward_qty' => $reward_qty,
        ];

        return $result;
    }

    private function getRewardSize(int $from, int $to): int
    {
        $rewardSize = rand($from, $to);

        return $rewardSize;
    }

    /**
     * @param Reward $reward
     * @return Thing
     * @throws \Exception
     */
    private function getAvailableThing(Reward $reward)
    {
        $thing = Thing::getNonGifted();

        try {
            $this->setThingUnavailable($reward, $thing);
        } catch (\Exception $e) {
            Yii::error([
                'message' => $e->getMessage()
            ]);
        }

        return $thing;
    }

    private function setThingUnavailable(Reward $reward, Thing $thing): void
    {
        Thing::updateAll(['gifted' => Thing::GIFTED], ['id' => $thing->id]);

        $this->updateGiftedQty($reward, 1);
    }

    private function updateGiftedQty(Reward $reward, int $qty): void
    {
        $available = 1;

        if ($reward->total <= $reward->gifted_qty + $qty && $reward->type != Reward::POINTS) {
            $available = 0;
        }

        Reward::updateAll([
            'gifted_qty' => $reward->gifted_qty + $qty,
            'available' => $available
        ], ['id' => $reward->id]);
    }

    private function createRewardUserAssociate(Reward $reward, User $user, int $qty = null, int $thing_id = null)
    {
        $link = new RewardUser();
        $link->reward_id = $reward->id;
        $link->user_id = $user->id;
        $link->thing_id = $thing_id;
        $link->qty = $qty;

        $link->save();
    }

}